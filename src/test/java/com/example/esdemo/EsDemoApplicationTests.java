package com.example.esdemo;

import com.example.esdemo.service.EService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

@SpringBootTest
class EsDemoApplicationTests {
    @Autowired
    private EService eService;

    @Test
    void contextLoads() {
    }

    @Test
    public void insertIndex() throws IOException {
        eService.insertIkIndex();
    }

    @Test
    public void searchIndex() throws IOException {
        eService.searchIndex();
    }

    @Test
    public void searchAggregation() throws IOException {
        eService.searchAggregation();
    }

    @Test
    public void searchSuggest() throws IOException {
        eService.searchSuggest("天");
    }

    @Test
    public void searchGeo() throws IOException {
        eService.searchGeo();
    }
}
