package com.example.esdemo.service;

import com.alibaba.fastjson.JSON;
import com.example.esdemo.model.IkIndex;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.geo.GeoPoint;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.common.unit.DistanceUnit;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.ParsedStringTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregator;
import org.elasticsearch.search.aggregations.metrics.AvgAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.StatsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.ValueCountAggregationBuilder;
import org.elasticsearch.search.aggregations.support.ValueType;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.FastVectorHighlighter;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.fetch.subphase.highlight.Highlighter;
import org.elasticsearch.search.sort.GeoDistanceSortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.elasticsearch.search.suggest.Suggest;
import org.elasticsearch.search.suggest.SuggestBuilder;
import org.elasticsearch.search.suggest.SuggestBuilders;
import org.elasticsearch.search.suggest.SuggestionBuilder;
import org.elasticsearch.search.suggest.completion.CompletionSuggestion;
import org.elasticsearch.search.suggest.completion.CompletionSuggestionBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class EService {
    @Autowired
    private RestHighLevelClient client;

    /**
     * 添加索引数据
     * @throws IOException
     */
    public void insertIkIndex() throws IOException {

        IkIndex ikIndex = new IkIndex();
        ikIndex.setId(10l);
        ikIndex.setTitle("足球");
        ikIndex.setDesc("足球是世界第一运动");
        ikIndex.setCategory("体育");

        IndexRequest request = new IndexRequest("ik_index");
//        request.id("1");
        request.source(JSON.toJSONString(ikIndex), XContentType.JSON);

        IndexResponse indexResponse = client.index(request, RequestOptions.DEFAULT);

        System.out.println(indexResponse.status());
        System.out.println(indexResponse.toString());
    }


    public void searchIndex() throws IOException {
        SearchRequest searchRequest = new SearchRequest("ik_index");
        SearchSourceBuilder ssb = new SearchSourceBuilder();
        QueryBuilder qb = new MatchQueryBuilder("desc","香蕉好吃");
        ssb.query(qb);

        HighlightBuilder highlightBuilder = new HighlightBuilder();
        highlightBuilder.field("desc");
        highlightBuilder.preTags("<b>");
        highlightBuilder.postTags("</b>");

        ssb.highlighter(highlightBuilder);

        searchRequest.source(ssb);
        SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);

        SearchHit[] hits = response.getHits().getHits();
        for (SearchHit hit : hits) {
            String record = hit.getSourceAsString();
            HighlightField highlightField = hit.getHighlightFields().get("desc");

            for (Text fragment : highlightField.getFragments()) {
                System.out.println(fragment.string());
            }
        }
    }

    public void searchAggregation() throws IOException {
        SearchRequest searchRequest = new SearchRequest("ik_index");
        SearchSourceBuilder ssb = new SearchSourceBuilder();

        TermsAggregationBuilder category = AggregationBuilders.terms("category").field("category.keyword");
        ssb.aggregation(category);

        searchRequest.source(ssb);
        SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);

        Terms terms = response.getAggregations().get("category");

        for (Terms.Bucket bucket : terms.getBuckets()) {
            System.out.println(bucket.getKey());
            System.out.println(bucket.getDocCount());
        }
    }

    public void searchSuggest(String prefix) throws IOException {
        SearchRequest searchRequest = new SearchRequest("my_suggester");
        SearchSourceBuilder ssb = new SearchSourceBuilder();

        CompletionSuggestionBuilder suggest = SuggestBuilders
                .completionSuggestion("suggest")
                .prefix(prefix);

        SuggestBuilder suggestBuilder = new SuggestBuilder();
        suggestBuilder.addSuggestion("s-test",suggest);

        ssb.suggest(suggestBuilder);

        searchRequest.source(ssb);
        SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);

        CompletionSuggestion suggestion = response.getSuggest().getSuggestion("s-test");
        for (CompletionSuggestion.Entry.Option option : suggestion.getOptions()) {
            System.out.println(option.getText().string());
        }

    }

    public void searchGeo() throws IOException {
        SearchRequest searchRequest = new SearchRequest("my_geo");
        SearchSourceBuilder ssb = new SearchSourceBuilder();

        //工体的坐标
        GeoPoint geoPoint = new GeoPoint(39.93367367974064d,116.47845257733152d);
        //geo距离查询  name=geo字段
        QueryBuilder qb = QueryBuilders.geoDistanceQuery("location")
                //距离 5KM
                .distance(5d, DistanceUnit.KILOMETERS)
                //坐标工体
                .point(geoPoint);

        GeoDistanceSortBuilder sortBuilder = SortBuilders
                .geoDistanceSort("location", geoPoint)
                .order(SortOrder.ASC);

        ssb.query(qb);
        ssb.sort(sortBuilder);
        searchRequest.source(ssb);
        SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);

        for (SearchHit hit : response.getHits().getHits()) {
            System.out.println(hit.getSourceAsString());
        }


    }

}
