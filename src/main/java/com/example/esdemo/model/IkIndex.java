package com.example.esdemo.model;

import lombok.Getter;
import lombok.Setter;
import org.elasticsearch.common.geo.GeoPoint;

@Setter@Getter
public class IkIndex {

    private Long id;
    private String title;
    private String desc;
    private String category;
    private GeoPoint geoPoint;

}
